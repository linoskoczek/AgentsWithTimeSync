# Clock sync between Agents in P2P network + Monitor

_Author of the project: Marcin Węgłowski, s15237_

_Requirements: local network, Java Runtime Environment 8_

## Overview

This project was to create a network of Agents who synchronise their clock values. Agents are connected in P2P. First agent is known as an _Introducing Agent_ and it's the agent to which other ones should connect firstly when starting.
There is also a Monitor, which allows you to add other agents and, as the name suggests, monitor them: order to synchronise their clocks, see their clock values or disconnect them.

More details about what the project should do are available in the task itself.

## Before you start

_It's assumed that `java` is a path to your java.exe file in Java Runtime Environment 8. In Linux you just have to have JRE-8 installed._

**Starting an Agent** from command line looks like that:
```
java Agent.Main <clk> <address> <port>
```

Where:

`clk` - initial value of the clock, any number bigger than 0

`address` - address of an Agent to which you want to connect, you should enter an address which is available to the other members of the network

`port` - server port of an Agent to which you want to connect

**Starting an Introducing Agent** is same, but without address and port:
```
java Agent.Main <clk>
```

## How to start example

```
java Agent.Main 123
java Agent.Main 500 192.168.0.10 10000
java Agent.Main 666 192.168.0.10 10000
```
Where:

`123, 500, 666` - any initial clock values at the beginning.

`192.168.0.10` - address of Introducing Agent

`10000` - port of Introducing Agent

## Monitor

After starting Agents (or actually whenever you want to) you can start a Monitor:
```
java ServerHTTP.MainMonitor
```

The Monitor's HTTP server by default starts at port 15000. Therefore, to enter the Monitor, go to your browser and type address of the computer you started server on and server's port number, for example:
```
http://192.168.0.15:1500
```

After you open Monitor, you have to manually add Agents which you want to monior. Simply enter their adresses and server port numbers to appropriate input boxes and press a button next to them.

In a moment, you will see the message in console (on the bottom of the Monitor) whether agent has been added or not. If it has been added, then you may now look at his clock, and use one of two buttons:

`SYN` - send command to this agent to synchronise it's clock

`DSC` - disconnect this agent from the network

Clocks are refreshed every 2 seconds by default. Right after adding an agent value of clock is 0 because the value of CLK hadn't arrived before the list was refreshed. In next refresh value of clock should be all right.

**Notice:** No file will be found if you try to run a program in IDE, because it will look for the files like index.html in wrong directory. Run the program using command line and everything will be fine.

## How to read console logs

`[RECEIVED] <cmd> <info>` - information about the command which Agent received. `cmd` stands for name of command, `info` is the information about what will be done now

`Sending <cmd>` - information about a command which was sent to some Agent

`Sending ANS:<cmd><msg>` - information about an answer which was sent to some Agent, `msg` stands for the content of answer

`[CLOCK]` - current value of clock

`[SYN]` - information about the process of clock synchronisation (notice: output prints 2/2, means it will base clock count on 2 agents + own one)

## What was (or wasn't) implemented

- [x] sharing structure of network between Agents
- [x] synchronisation of clocks after some Agent connect to the network
- [x] synchronisation of clocks after an Agent disconnects
- [x] updating structure of network when an Agent disconnects
- [x] monitor to see changes in clocks
- [x] monitor running as a HTTP server
- [x] SYN button to send SYN message to selected agent, DSC to disconnect selected agent
- [x] table of Agents with their addresses and server ports
- [x] setting clock values to the ones that are given by argument
- [x] sharing IP addresses which allow connections from other computers in the same local network
- [ ] code of Agent could be improved, several things are more complicated than they should be

## Bugs and weaknesses
- no serious bugs found
- anyway if any found, consider them as a feature
- CLK sync doesn't look at latency problems - if put out of local network it would make significant change in clocks
- no protection against understandable command floods
- time in miliseconds stored in long variables - if Agents would have to work for longer time, consider changing miliseconds to smaller (in size of number) unit
- don't consider console log order very seriously...

_Project was created during process of education on PJATK._

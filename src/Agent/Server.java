package Agent;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {
    static int defaultIntroducingAgentPort = 10000;
    static boolean isIntroducingAgent = false;
    static ServerSocket serverSocket = null;

    public Server(int port) {
        System.out.println("Hello, I'm an introducing Agent!");
        defaultIntroducingAgentPort = port;
        isIntroducingAgent = true;
        createServerSocket(defaultIntroducingAgentPort);
    }

    public Server() {
        System.out.println("Hello, I'm an Agent!");
        createServerSocket(-1);
    }

    private void createServerSocket(int port) {
        try {
            System.out.print("Creating a server socket... ");
            if (port == -1)
                serverSocket = new ServerSocket(0);
            else
                serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("ERROR");
            e.printStackTrace();
        }
        System.out.println("OK, taken port " + serverSocket.getLocalPort());
    }

    ServerSocket getServerSocket() {
        return serverSocket;
    }
}

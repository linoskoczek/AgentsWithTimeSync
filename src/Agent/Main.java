package Agent;

import java.io.IOException;
import java.net.Socket;

public class Main {
    private static Server server;

    public static void main(String args[]) throws IOException {
        startServer(args);
        connectToIntroducingAgent(args);
        startClock(args);
        startListening();
    }

    private static void startListening() throws IOException {
        while (true) {
            Socket clientSocket = server.getServerSocket().accept();
            new Client(clientSocket);
        }
    }

    private static void startServer(String[] args) {
        if (args.length != 3) //will be introducing agent
            server = new Server(10000); //here you can decide about the default port of introducing agent
        else
            server = new Server();
    }

    private static void connectToIntroducingAgent(String[] args) {
        if (Server.isIntroducingAgent) return;
        System.out.println("Trying to connect to introducing agent... ");
        Connection con = new Connection(args[1], Integer.parseInt(args[2]));

        if (con.hadError) System.exit(1);

        AgentInfo agentInfo = new AgentInfo(con.getClientSocket(),
                con.getClientSocket().getInetAddress().toString(),
                Integer.parseInt(args[2]),
                con.getSender(),
                con.getReceiver()
        );
        AgentStorage.add(agentInfo);
    }

    private static void startClock(String[] args) {
        if (args.length < 1) {
            System.out.println("Provide starting clock time as an argument!");
            System.exit(2);
        }
        Clock.initialState = Long.parseLong(args[0]);
        Clock.startClock();
    }

}

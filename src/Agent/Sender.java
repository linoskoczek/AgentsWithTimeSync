package Agent;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

class Sender {
    boolean confirmed = false;
    private Socket clientSocket;
    private DataOutputStream dOut;

    Sender(Socket clientSocket) {
        this.clientSocket = clientSocket;
        try {
            this.dOut = new DataOutputStream(clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        sleep(500);
    }

    static void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    void sendMessage(String message) {
        System.out.println("Sending " + message);
        try {
            dOut.writeUTF(message);
            confirmed = true;
        } catch (IOException e) {
            System.out.println("Error while sending a message.");
            e.printStackTrace();
        }
    }

    void sendAnswer(String text, String cmd) {
        sendMessage("ANS:" + cmd + text);
    }

    Socket getClientSocket() {
        return clientSocket;
    }
}

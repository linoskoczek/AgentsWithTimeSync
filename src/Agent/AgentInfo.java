package Agent;

import java.net.Socket;

public class AgentInfo {
    private Socket clientSocket;

    private String hostname;
    private int serverPort = 0;

    private Sender sender;
    private Receiver receiver;

    public AgentInfo(Socket clientSocket, String hostname, int serverPort, Sender sender, Receiver receiver) {
        this.clientSocket = clientSocket;
        this.hostname = hostname;
        this.serverPort = serverPort;
        this.sender = sender;
        this.receiver = receiver;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public String getHostname() {
        return hostname;
    }

    public int getServerPort() {
        return serverPort;
    }

    public Sender getSender() {
        return sender;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public int getPort() {
        return clientSocket.getPort();
    }

}

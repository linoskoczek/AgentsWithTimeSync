package Agent;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Connection {
    public boolean hadError = false;
    private Socket clientSocket;
    private Receiver receiver;
    private Sender sender;

    public Connection(String hostname, int port) {
        createConnection(hostname, port);
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public Sender getSender() {
        return sender;
    }

    private void createConnection(String hostname, int port) {
        try {
            clientSocket = new Socket(hostname, port);
        } catch (UnknownHostException e) {
            System.err.println("Unknown host: " + hostname + ".");
            hadError = true;
        } catch (IOException e) {
            System.err.println("Cannot connect to " + hostname + ".");
            if (Server.isIntroducingAgent) System.exit(1);
            hadError = true;
        }

        startReceiverAndSenderThreads();
    }

    private void startReceiverAndSenderThreads() {
        sender = Functions.createSender(clientSocket);
        receiver = Functions.createReceiver(clientSocket, sender);

        while (!sender.confirmed) {
            Sender.sleep(5);
        }
        sender.confirmed = false;
        if (this.clientSocket.getPort() == Server.defaultIntroducingAgentPort) {
            this.sender.sendMessage("NET");
        }
    }

    public Socket getClientSocket() {
        return clientSocket;
    }
}
package Agent;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

public class AgentStorage {
    public static ArrayList<AgentInfo> storage = new ArrayList<>();

    synchronized static void add(AgentInfo agentInfo) {
        if (!storage.contains(agentInfo)) storage.add(agentInfo);
    }

    synchronized static void remove(Socket clientSocket) {
        final int port = clientSocket.getPort();
        storage.removeAll(storage.stream()
                .filter(Objects::nonNull)
                .filter(e -> e.getPort() == port && e.getHostname().equals(clientSocket.getInetAddress().toString()))
                .collect(Collectors.toList()));
    }

    synchronized static String getContentAsString() {
        StringBuilder show = new StringBuilder();
        for (AgentInfo agentInfo : storage) {

            show.append(agentInfo.getHostname()).append(":")
                    .append(agentInfo.getServerPort())
                    .append(",");
        }
        return show.toString();
    }

    synchronized public static String findAndSendMessage(String hostname, int port, String message) {
        storage.stream()
                .filter(Objects::nonNull)
                .filter(e -> e.getPort() == port && e.getHostname().equals(hostname))
                .forEach(e -> e.getSender().sendMessage(message));
        return message + " sent!";
    }
}

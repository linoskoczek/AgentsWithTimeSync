package Agent;

import java.net.Socket;

class Functions {

    static Receiver createReceiver(Socket clientSocket, Sender sender) {
        Receiver receiver = new Receiver(clientSocket, sender);
        Thread thread = new Thread(receiver);
        thread.start();

        return receiver;
    }

    static Sender createSender(Socket clientSocket) {
        return new Sender(clientSocket);
    }

}

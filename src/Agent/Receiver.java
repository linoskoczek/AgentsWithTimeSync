package Agent;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

public class Receiver extends Commander implements Runnable {
    public Socket clientSocket;
    private DataInputStream dIn;

    Receiver(Socket clientSocket, Sender sender) {
        this.clientSocket = clientSocket;
        this.sender = sender;
        this.receiver = this;
        try {
            dIn = new DataInputStream(clientSocket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            System.out.println("Starting receiver for " + clientSocket);
            String input;
            while (true) {
                input = dIn.readUTF();
                if (!input.isEmpty()) {
                    if (input.length() == 3)
                        command(input);
                    else if (input.startsWith("ANS:"))
                        answer(input);
                }
            }
        } catch (SocketException | EOFException e) {
            System.out.println("Disconnected from " + clientSocket);
            AgentStorage.remove(clientSocket);
            synchronisationStep();
            System.out.println("[DATABASE]: " + AgentStorage.getContentAsString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

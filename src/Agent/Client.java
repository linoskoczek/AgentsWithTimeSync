package Agent;

import java.net.InetAddress;
import java.net.Socket;

public class Client {
    private Socket clientSocket;
    private Receiver receiver;
    private Sender sender;

    Client(Socket clientSocket) {
        this.clientSocket = clientSocket;
        startReceiverAndSenderThreads();
        sender.sendMessage("HEY");
    }

    private void startReceiverAndSenderThreads() {
        sender = Functions.createSender(clientSocket);
        receiver = Functions.createReceiver(clientSocket, sender);
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public Sender getSender() {
        return sender;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public InetAddress getAddress() {
        return clientSocket.getInetAddress();
    }

    public int getPort() {
        return clientSocket.getPort();
    }
}
package Agent;

import java.util.Arrays;

import static Agent.Clock.getClock;

abstract class Commander {
    private volatile static int receivedClocksCounter;
    private volatile static long clockSum;
    Sender sender;
    Receiver receiver;
    AgentInfo agent;
    private boolean firstTime = true;

    void command(String input) { // command looks like that: CMD
        switch (input) {
            case "CLK":
                System.out.println("[RECEIVED] CLK. Sending back my clock value");
                sender.sendAnswer(Long.toString(getClock()), input);
                break;
            case "SYN":
                System.out.println("[RECEIVED] SYN. Starting to perform synchronisation step.");
                synchronisationStep();
                break;
            case "NET":
                System.out.println("[RECEIVED] NET. Sending back database of agents");
                String netAnswer = AgentStorage.getContentAsString();
                sender.sendAnswer(netAnswer, input);
                break;
            case "HEY":
                System.out.println("[RECEIVED] HEY. Sending back my server port");
                sender.sendAnswer(Integer.toString(Server.serverSocket.getLocalPort()), input);
                sender.confirmed = true;
                break;
            case "DSC":
                System.exit(0);
                break;
        }
    }

    void answer(String input) { // answer looks like that: ANS:CMDmessage_here
        System.out.println("[RECEIVED] " + input);
        input = input.substring(4);
        String cmd = input.substring(0, 3);
        input = input.substring(3);
        switch (cmd) {
            case "CLK":
                receivedClocksCounter++;
                long clock = Long.parseLong(input);
                addToClkSum(clock);
                break;
            case "NET":
                connectToReceivedAgentList(input);
                if (firstTime) {
                    firstTime = false;
                    firstTimeSynchronisationStep();
                }
                break;
            case "HEY":
                agent = new AgentInfo(sender.getClientSocket(),
                        sender.getClientSocket().getLocalAddress().toString(),
                        Integer.parseInt(input),
                        sender,
                        receiver
                );
                AgentStorage.add(agent);
                break;
        }
    }

    private void firstTimeSynchronisationStep() {
        synchronisationStep();
        sendSYNtoOthers();
    }

    private void sendSYNtoOthers() {
        AgentStorage.storage.forEach(e -> e.getSender().sendMessage("SYN"));
    }

    void synchronisationStep() {
        receivedClocksCounter = 0;
        clockSum = 0;
        int howMany = AgentStorage.storage.size();
        AgentStorage.storage.forEach(e -> e.getSender().sendMessage("CLK"));
        Thread thread = new Thread(() -> {
            int timeout = 5000;
            int alreadyWaited = 0;
            while (receivedClocksCounter < howMany) {
                Sender.sleep(30);
                alreadyWaited += 30;
                if (alreadyWaited >= timeout) break;
            }
            if (alreadyWaited >= timeout) System.out.println("[SYN] Timeout. Clocks won't be synchronised perfectly.");
            if (receivedClocksCounter > 0) {
                System.out.println("[SYN] " + receivedClocksCounter + " / " + howMany + " succeeded.");
                clockSum += Clock.getClock();
                clockSum /= (receivedClocksCounter + 1);
                long diff = Clock.getClock() - clockSum;
                Clock.initialState -= diff;
                System.out.println("[SYN] Clock changed to " + Clock.getClock());
            }
        });
        thread.start();
    }

    private synchronized void addToClkSum(long num) {
        clockSum += num;
    }

    private void connectToReceivedAgentList(String input) {
        int serverPort = Server.serverSocket.getLocalPort();

        Arrays.stream(input.substring(0, input.length() - 1).split(",")).forEach(e -> {
            String[] singleAgent = e.split(":");

            String host = singleAgent[0].charAt(0) == '/' ? singleAgent[0].substring(1, singleAgent[0].length()) : singleAgent[0];
            int port = Integer.parseInt(singleAgent[1]);

            if (port != serverPort && port != Server.defaultIntroducingAgentPort) {
                Connection con = new Connection(host, port);
                if (!con.hadError) {
                    agent = new AgentInfo(con.getClientSocket(),
                            con.getClientSocket().getLocalAddress().toString(),
                            port,
                            con.getSender(),
                            con.getReceiver()
                    );
                    AgentStorage.add(agent);
                }
            }
        });
    }
}

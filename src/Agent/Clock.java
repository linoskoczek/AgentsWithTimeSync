package Agent;

class Clock {
    volatile static long initialState = 0;
    volatile private static long hiddenClockStartTime;

    static void startClock() {
        hiddenClockStartTime = System.currentTimeMillis();
        printClockValue();
    }

    static long getClock() {
        return System.currentTimeMillis() - hiddenClockStartTime + initialState;
    }

    static void printClockValue() {
        System.out.println("[CLOCK] " + getClock());
    }


}

function loadData(file) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementsByTagName("section")[0].innerHTML = this.responseText;
        }
    };
    xhttp.open("GET", file, true);
    xhttp.send();
}

function syn(hostname, port) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementsByTagName("code")[0].innerHTML += getDate() + this.responseText + "<br>";
        }
    };
    xhttp.open("GET", "SYN"+hostname+"-"+port, true);
    xhttp.send();
}

function add(hostname, port) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementsByTagName("code")[0].innerHTML += getDate() + this.responseText + "<br>";
            document.getElementById("hostname").value = "";
            document.getElementById("port").value = "";
        }
    };
    xhttp.open("GET", "ADD"+hostname+"-"+port, true);
    xhttp.send();
}

function dsc(hostname, port) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementsByTagName("code")[0].innerHTML += getDate() + this.responseText + "<br>";
            document.getElementById("hostname").value = "";
            document.getElementById("port").value = "";
        }
    };
    xhttp.open("GET", "DSC"+hostname+"-"+port, true);
    xhttp.send();
}

function getDate() {
    var currentdate = new Date();
    return "[" + currentdate.getDate() + "/"
        + (currentdate.getMonth()+1)  + "/"
        + currentdate.getFullYear() + " - "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds() + "] ";
}


loadData('data.html');
window.setInterval(function() {
    loadData('data.html');
}, 2000);
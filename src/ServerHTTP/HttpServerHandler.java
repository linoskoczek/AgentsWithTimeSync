package ServerHTTP;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

class HttpServerHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange t) throws IOException {
        String requestURI = t.getRequestURI().toString();
        String response;
        response = handleRequest(requestURI);
        t.sendResponseHeaders(200, response.length());
        OutputStream os = t.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    private String handleRequest(String requestURI) {
        String response;
        switch (requestURI) {
            case ServerUtilities.homeDir:
                response = ServerUtilities.readFile(ServerUtilities.indexFile);
                break;
            case ServerUtilities.agentDataFile:
                response = generateAgentData();
                break;
            default:
                String cmd = requestURI.substring(1, 4);
                String rest = requestURI.substring(4, requestURI.length());
                String[] splitted = rest.split("-");
                switch (cmd) {
                    case "SYN":
                        if (splitted.length < 2 || Integer.parseInt(splitted[1]) == ServerUtilities.serverPort)
                            return "Wrong data given.";
                        response = Monitor.synchronize(splitted[0], Integer.parseInt(splitted[1]));
                        break;
                    case "ADD":
                        if (splitted.length < 2 || Integer.parseInt(splitted[1]) == ServerUtilities.serverPort)
                            return "Wrong data given.";
                        response = Monitor.connectTo(splitted[0], Integer.parseInt(splitted[1]));
                        break;
                    case "DSC":
                        if (splitted.length < 2 || Integer.parseInt(splitted[1]) == ServerUtilities.serverPort)
                            return "Wrong data given.";
                        response = Monitor.disconnect(splitted[0], Integer.parseInt(splitted[1]));
                        break;
                    default:
                        response = ServerUtilities.readFile(requestURI);
                }
        }
        return response;
    }

    private String generateAgentData() {
        StringBuilder sb = new StringBuilder();
        sb.append("<table><tr><th>Hostname</th><th>Server port</th><th>Clock value</th></tr>");
        Monitor.agentList.stream()
                .filter(Objects::nonNull)
                .forEach(a -> sb.append("<tr><td>").append(a.hostname).append("</td><td>").append(a.port)
                        .append("</td><td>").append(a.clock).append("</td>")
                        .append("<td><button onclick='syn(\"").append(a.hostname).append("\",").append(a.port)
                        .append(")'>SYN</button><button onclick='dsc(\"").append(a.hostname)
                        .append("\",").append(a.port).append(")'>DSC</button></td></tr>"));
        sb.append("</table>");
        return sb.toString();
    }
}
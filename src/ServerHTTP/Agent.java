package ServerHTTP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class Agent {
    Socket clientSocket;
    String hostname;
    int port;
    DataOutputStream dOut;
    DataInputStream dIn;
    volatile long clock;

    public Agent(Socket clientSocket, String hostname, int port) {
        this.clientSocket = clientSocket;
        this.hostname = hostname;
        this.port = port;
    }
}

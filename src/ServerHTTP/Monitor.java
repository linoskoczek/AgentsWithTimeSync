package ServerHTTP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Thread.sleep;

public class Monitor {

    static List<Agent> agentList = new ArrayList<>();
    private static boolean clockUpdaterStarted = false;

    static String connectTo(String hostname, int port) {
        try {
            Socket clientSocket = new Socket(hostname, port);
            Agent agent = new Agent(clientSocket, hostname, port);
            agentList.add(agent);
            startListener(agent);
            startSender(agent);
            startClockUpdater();
            return "Connected to " + hostname + ":" + port;
        } catch (IOException e) {
            return "Could not connect to " + hostname + ":" + port;
        }
    }

    static void startClockUpdater() {
        if (clockUpdaterStarted) return;
        Thread thread = new Thread(() -> {
            while (true) {
                if (agentList.size() > 0) {
                    agentList.forEach(a -> sendMessage("CLK", a));
                }
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private static void startSender(Agent agent) {
        try {
            agent.dOut = new DataOutputStream(agent.clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            sleep(500);
        } catch (InterruptedException e) {
        }
    }

    private static void startListener(Agent agent) {
        try {
            agent.dIn = new DataInputStream(agent.clientSocket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread thread = new Thread(() -> {
            try {
                agent.dOut = new DataOutputStream(agent.clientSocket.getOutputStream());
                sleep(500);
                System.out.println("Starting receiver for " + agent.clientSocket);
                String input;
                while (true) {
                    input = agent.dIn.readUTF();
                    if (!input.isEmpty() && input.startsWith("ANS:")) {
                        answer(input, agent);
                    }
                }
            } catch (IOException | InterruptedException e) {
                System.out.println("Disconnected from " + agent.clientSocket);
                agentList.remove(agent);
            }
        });
        thread.start();
    }

    private static void answer(String input, Agent agent) {
        String cmd = input.substring(4, 7);
        String data = input.substring(7, input.length());

        switch (cmd) {
            case "CLK":
                agent.clock = Long.parseLong(data);
                System.out.println("Received clock: " + agent.clock);
        }
    }

    private static void sendMessage(String message, Agent agent) {
        System.out.println("Sending " + message);
        try {
            agent.dOut.writeUTF(message);
        } catch (IOException e) {
            System.out.println("Error while sending a message.");
            e.printStackTrace();
        }
    }

    static String synchronize(String hostname, int port) {
        agentList.stream()
                .filter(a -> a.port == port && a.hostname.equals(hostname))
                .forEach(a -> sendMessage("SYN", a));
        return "SYN sent!";
    }

    static String disconnect(String hostname, int port) {
        List<Agent> foundAgents = agentList.stream()
                .filter(a -> a.hostname.equals(hostname) && a.port == port)
                .collect(Collectors.toList());

        foundAgents.forEach(a -> sendMessage("DSC", a));

        agentList.removeAll(foundAgents);

        if (foundAgents.size() > 0) return "Disconnected this agent!";
        else return "Agent could not be disconnected.";
    }

}

package ServerHTTP;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

class MainMonitor {
    private static HttpServer server;

    public static void main(String[] args) throws IOException {
        createHttpServer();
    }

    private static void createHttpServer() {
        try {
            server = HttpServer.create(new InetSocketAddress(ServerUtilities.serverPort), 0);
            server.createContext(ServerUtilities.homeDir, new HttpServerHandler());
            server.setExecutor(null);
            server.start();
        } catch (IOException e) {
            System.out.println("Could not start HTTP server.");
            System.exit(1);
        } finally {
            System.out.println("Http server started on port " + ServerUtilities.serverPort);
        }
    }

}
package ServerHTTP;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ServerUtilities {
    static final int serverPort = 15000;
    static final String error404 = "<h1>404 - Not Found</h1>";
    static final String error403 = "<h1>403 - Forbidden</h1>";
    static final String wwwDir = System.getProperty("user.dir") + "/WWW";
    static final String homeDir = "/";
    static final String indexFile = "/index.html";
    static final String agentDataFile = "/data.html";

    static String readFile(String name) {
        FileReader fileReader;
        BufferedReader bufferedReader;
        try {
            fileReader = new FileReader(wwwDir + name);
            bufferedReader = new BufferedReader(fileReader);

            String line;
            StringBuilder file = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                file.append(line);
            }

            fileReader.close();
            bufferedReader.close();

            return file.toString();
        } catch (FileNotFoundException e) {
            System.out.println("File " + wwwDir + name + " was not found");
            return error404;
        } catch (IOException e) {
            return error403;
        }
    }
}
